FROM python:3.7.8-alpine
WORKDIR /app
RUN apk add --update --no-cache build-base
COPY requirements.min.txt ./
RUN PIP_DISABLE_PIP_VERSION_CHECK=1 PIP_NO_CACHE_DIR=1 pip install -r requirements.min.txt
RUN apk del build-base
COPY . .

# note: -S means system group/user
RUN addgroup -S modmailg && adduser -S -G modmailg modmail
RUN chown modmail:modmailg /app
USER modmail

CMD ["python", "bot.py"]
